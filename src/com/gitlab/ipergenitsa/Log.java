package com.gitlab.ipergenitsa;

public class Log {
    public String info(String message) {
        return log("info", message);
    }

    public String debug(String message) {
        return log("debug", message);
    }

    private String log(String level, String message) {
        return String.format("[%s] %s", level, message);
    }
}
